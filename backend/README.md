### Demo
[https://sellics-api.herokuapp.com/images](https://sellics-api.herokuapp.com/images)

### Getting Started
1. `cd` into `sellics-project/backend`
2. run `npm install`
3. run `node server.js`

### Available routes and methods
`GET` request `/images`
