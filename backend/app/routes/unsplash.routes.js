module.exports = (app) => {
    const images = require('../controllers/unsplash.controller');

    //routes for images
    app.get('/images', images.findAll);
};
