const createApi = require('unsplash-js').createApi;
const nodeFetch = require('node-fetch');

const serverApi = createApi({
    accessKey: process.env.API_KEY,
    fetch: nodeFetch,
});

exports.findAll = async (req, res) => {
    getUnsplashImages()
        .then((response) => {
            res.send(response)
        })
        .catch((err) => {
            res.status(500).send({
                message: err.message || "Something went wrong. Try again."
            });
        });

};

//retrieve random images from unsplash
function getUnsplashImages() {
    return new Promise((resolve,reject) => {
        serverApi.photos.getRandom({count: 30})
            .then(res => {
                resolve(res);
            }).catch(err => {
                reject(err);
        })
    })
}
