require("dotenv").config();
const express = require('express');
const app = express();
const cors =  require('cors');

app.use(cors());
//initialize middleware
app.use(express.json({ extended: false }));

require('./app/routes/unsplash.routes')(app);

app.listen(process.env.PORT, () => {
    console.log(`Server is listening on port ${process.env.PORT}`);
});
