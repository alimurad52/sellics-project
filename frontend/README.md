### Demo
[https://sellics.ali-murad.com/](https://sellics.ali-murad.com/)

### Project setup
1. Run `git clone https://alimurad52@bitbucket.org/alimurad52/sellics-project.git`
2. `cd` into `sellics-project/frontend`
3. run `npm install`
4. run `npm start`

### Test cases
1. `cd` into `sellics-project/frontend`
2. run `npm run test`