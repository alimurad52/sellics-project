import React, {useEffect, useState, useCallback} from 'react';
import ApprovedImagesContainer from "../components/ApprovedImagesContainer";
import styled from "styled-components";
import ProposedImageContainer from "../components/ProposedImageContainer";
import Divider from "../components/Divider/Divider";
import Footer from "../components/Footer/Footer";
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {approvedImages, rejectedImages} from "../store/actions/imageAction";

const Home = () => {
    const [imageList, setImageList] = useState([]);
    const [showLoading, setShowLoading] = useState(false);
    const rejectedImagesList = useSelector(state => state.images.rejectedImages);
    const dispatch = useDispatch();

    // use callback to memoize the function
    const getImages = useCallback(() => {
        setShowLoading(true);
        axios.get('https://sellics-api.herokuapp.com/images')
            .then(res => {
                if(res.data.status === 200) {
                    // filter the response against the ids to not have any
                    // image that was previously rejected
                    let list = res.data.response.filter(el => !rejectedImagesList.includes(el.id));
                    // concat the incoming array with the existing list
                    setImageList([...imageList, ...list]);
                    setShowLoading(false);
                } else {
                    // if request fails, preserve the original list
                    setImageList(imageList);
                }
            }).catch(err => {
            // if request fails, preserve the original list
            setImageList(imageList);
        })
    }, [imageList, rejectedImagesList]);

    useEffect(() => {
        // when the image list has less than 5 items left fetch the next 30 items
        // to avoid any fetching time on the frontend
        if(imageList.length > 0 && imageList.length < 5) {
            getImages();
        }
    },[imageList, getImages])

    const removeImageFromArray = () => {
        // create a copy of the original array
        let imageListCc = imageList.slice();
        // remove the first item from the array which is last displayed
        imageListCc.shift()
        setImageList(imageListCc);
    };

    const onClickButton = (type) => {
        const clickedImg = imageList[0];
        let image = {
            url: clickedImg.urls.regular,
            alt: clickedImg.alt_description,
            id: clickedImg.id
        };
        if(type === "approve") dispatch(approvedImages(image));
        if(type === "reject") dispatch(rejectedImages(clickedImg.id));
        removeImageFromArray();
    };

    return (
        <HomeWrapper>
            <ApprovedImagesContainer
                showLoading={showLoading}
                onClickPlus={getImages}
            />
            <Divider />
            <ProposedImageContainer
                showLoading={showLoading}
                imageList={imageList}
                getImages={getImages}
            />
            <Divider />
            <Footer
                onClickButton={onClickButton}
                showButtons={imageList.length > 0}
            />
        </HomeWrapper>
    )
}

const HomeWrapper = styled.div`
    padding: 0 15px;
`;

export default Home;