export const PrimaryColor = '#1a59bd';
export const DividerColor = '#cfcfcf';
export const BgColor = '#f5f5f5';
export const BorderColor = '#dbdbdb';
export const FadedBgColor = 'rgba(244, 244, 244, 0.7)';

export const smallImageHeight = 64;
export const smallImageWidth = 104;