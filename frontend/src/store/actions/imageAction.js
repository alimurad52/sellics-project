import {APPROVED_IMAGES, REJECTED_IMAGES} from "../actionTypes";

//actions that are directly called and dispatched in the components
export const approvedImages = (data) => {
    return dispatch => {
        dispatch(setApproveImages(data));
    }
};

export const rejectedImages = (data) => {
	return dispatch => {
		dispatch(setRejectedImages(data));
	}
}

//functions that dispatches the action type to pass
//the payload to the reducer
function setApproveImages(payload) {
    return {
        type: APPROVED_IMAGES,
        payload: payload
    }
}

function setRejectedImages(payload) {
	return {
		type: REJECTED_IMAGES,
		payload: payload
	}
};