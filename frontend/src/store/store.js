import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import reducers from './reducers/rootReducer';
import storage from 'redux-persist/lib/storage';

//create the persist store
const persistConfig = {
    key: 'sellics',
    storage
};

//create the enhanced reducer that wraps the root reducer
//for the purpose of persisting the states
const pReducer = persistReducer(persistConfig, reducers);

export const store = createStore(pReducer, applyMiddleware(thunk));
export const persistor = persistStore(store);
