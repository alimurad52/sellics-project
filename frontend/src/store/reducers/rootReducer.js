import { combineReducers } from "redux";
import imageReducer from "./imageReducer";

//create and assign the form reducer object
const rootReducer = combineReducers({
    images: imageReducer
});
export default rootReducer;
