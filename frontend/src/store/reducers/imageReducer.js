import {APPROVED_IMAGES, REJECTED_IMAGES} from "../actionTypes";

const INITIAL_STATE = {
    approvedImages: [],
    rejectedImages: []
};

const imageReducer =  (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case APPROVED_IMAGES:
            return Object.assign({}, state, {
                approvedImages: [...state.approvedImages, action.payload]
            })
        case REJECTED_IMAGES:
            return Object.assign({}, state, {
                rejectedImages: [...state.rejectedImages, action.payload]
            })

        default:
            return state;
    }
}

export default imageReducer;