import React from "react";
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Footer from "./Footer";

configure({adapter: new Adapter()});

describe("unit test for footer", () => {
    it("footer reject button", () => {
        let wrapper = shallow(<Footer showButtons={true} onClickButton={() => {}} />);
        expect(wrapper.find('.reject-button')).toHaveLength(1);
    })
    it("footer approve button", () => {
        let wrapper = shallow(<Footer showButtons={true} onClickButton={() => {}} />);
        expect(wrapper.find('.approve-button')).toHaveLength(1);
    })
})