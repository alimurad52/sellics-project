import React from 'react';
import styled from "styled-components";
import {ReactComponent as Plus} from "../../assets/plus-solid.svg";
import {ReactComponent as Check} from "../../assets/check-solid.svg";
import {ReactComponent as Times} from "../../assets/times-solid.svg";
import {BorderColor, PrimaryColor} from "../../utils/const";

;

const Footer = (
    {
        showButtons,
        onClickButton
    }) => {
    return (
        <FooterWrapper>
            {
                !showButtons ?
                    <Paragraph>Click on the <StyledSVG /> in order to get image recommendation.</Paragraph> :
                    <ButtonWrapper>
                        <Button
                            className="reject-button"
                            type="reject"
                            onClick={onClickButton.bind(null, "reject")}
                        >
                            <Times />
                        </Button>
                        <Button
                            className="approve-button"
                            type="approve"
                            onClick={onClickButton.bind(null, "approve")}
                        >
                            <Check />
                        </Button>
                    </ButtonWrapper>
            }
        </FooterWrapper>
    )
}

const FooterWrapper = styled.div`
    padding: 25px 0;
    text-align: center;
`;

const Paragraph = styled.p`
    font-size: 18px;
`;

const StyledSVG = styled(Plus)`
    height: 18px;
    width: 18px;
    path {
        fill: ${BorderColor};
    }
`;

const Button = styled.button`
    background: ${(props) => props.type === "approve" ? PrimaryColor : "#36454f"};
    width: 50%;
    height: 50px;
    border-radius: 15px;
    cursor: pointer;
    svg {
        width: 25px;
        height: 25px;
        path {
            fill: white;
        }
    }
`;

const ButtonWrapper = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    gap: 50px;
`;

export default Footer;