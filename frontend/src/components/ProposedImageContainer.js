import React from 'react';
import Placeholder from "./Placeholder/Placeholder";
import ImageContainer from "./ImageContainer";

const ProposedImageContainer = ({imageList, getImages, showLoading}) => {
    return (
        <div>
            {
                imageList.length > 0 ?
                    <ImageContainer imageList={imageList} /> :
                    <Placeholder showLoading={showLoading} onClick={getImages} type="proposed" />
            }
        </div>
    )
};

export default ProposedImageContainer;