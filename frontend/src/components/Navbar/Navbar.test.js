import React from "react";
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Navbar from "./Navbar";

configure({adapter: new Adapter()});

describe("unit test for navbar", () => {
    it("contains heading", () => {
        let wrapper = shallow(<Navbar />);
        expect(wrapper.find('.navbar-heading')).toHaveLength(1);
    })
})