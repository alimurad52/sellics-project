import React from 'react';
import styled from "styled-components";
import {DividerColor, PrimaryColor} from "../../utils/const";

const Navbar = () => {
    return (
        <Nav>
            <ContentWrapper>
                <Paragraph className="navbar-heading">
                    Image approval application
                </Paragraph>
            </ContentWrapper>
        </Nav>
    )
};

const Nav = styled.nav`
    border-bottom: 1px solid ${DividerColor};
    height: 50px;
    z-index: 9;
    padding: 0 15px;
`;
const ContentWrapper = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    height: inherit;
`;
const Paragraph = styled.h3`
    margin: 0;
    font-size: 18px;
    color: ${PrimaryColor};
    text-transform: uppercase;
`;

export default Navbar;