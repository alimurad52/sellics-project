import React from "react";
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Placeholder from "./Placeholder";

configure({adapter: new Adapter()});

describe("unit test for placeholder", () => {
    it("renders loading icon when loading enabled", () => {
        let wrapper = shallow(<Placeholder showLoading={true} />);
        expect(wrapper.find('.loading-icon')).toHaveLength(1);
    })
})