import React from 'react';
import styled, {keyframes} from "styled-components";
import {ReactComponent as Plus} from "../../assets/plus-solid.svg";
import {ReactComponent as Loading} from "../../assets/spinner-solid.svg";
import {BgColor, BorderColor, smallImageHeight, smallImageWidth} from "../../utils/const";

const Placeholder = ({type, onClick, showLoading}) => {
    return (
        <PlaceholderWrapper type={type}>
            {
                showLoading ?
                    <StyledLoading type={type} className="loading-icon" /> :
                    <StyledSVG type={type} onClick={onClick} />

            }
        </PlaceholderWrapper>
    )
};

const PlaceholderWrapper = styled.div`
    background-color: ${BgColor};
    border-radius: 8px;
    border: 1px solid ${BorderColor};
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    width: ${(props) => props.type === "proposed" ? "100%" : `${smallImageWidth}px`};
    height: ${(props) => props.type === "proposed" ? "500px" : `${smallImageHeight}px`};
`;

const StyledSVG = styled(Plus)`
    height: ${(props) => props.type === "proposed" ? "60px" : "24px"};
    width: ${(props) => props.type === "proposed" ? "60px" : "24px"};
    path {
        fill: ${BorderColor};
    }
`;

const rotatingAnimation = keyframes`
    from {
        -ms-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -webkit-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
    }
    to {
        -ms-transform: rotate(360deg);
        -moz-transform: rotate(360deg);
        -webkit-transform: rotate(360deg);
        -o-transform: rotate(360deg);
        transform: rotate(360deg);
    }
`;

const StyledLoading = styled(Loading)`
    height: ${(props) => props.type === "proposed" ? "60px" : "24px"};
    width: ${(props) => props.type === "proposed" ? "60px" : "24px"};
    color: ${BorderColor};
    animation: ${rotatingAnimation} 2s linear infinite;
`;

export default Placeholder;