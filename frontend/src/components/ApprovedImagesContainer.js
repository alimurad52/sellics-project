import React, {createRef, useEffect, useState} from 'react';
import styled from "styled-components";
import {BgColor, BorderColor, FadedBgColor, PrimaryColor, smallImageHeight, smallImageWidth} from "../utils/const";
import Placeholder from "./Placeholder/Placeholder";
import {useSelector} from "react-redux";
import {ReactComponent as Check} from "../assets/check-solid.svg";
import {ReactComponent as ChevronRight} from "../assets/chevron-right-solid.svg";
import {ReactComponent as ChevronLeft} from "../assets/chevron-left-solid.svg";

const ApprovedImagesContainer = ({onClickPlus, showLoading}) => {
    const approvedImages = useSelector((state) => state.images.approvedImages);
    const listRef = createRef();
    const [hideArrowRight, setHideArrowRight] = useState(false);
    const [showArrowLeft, setShowArrowLeft] = useState(false);
    const [canShowRightArrow, setCanShowArrow] = useState(false);

    const onClickChevronRight = () => {
        let scrollLeft = listRef.current.scrollLeft;
        listRef.current.scrollLeft = scrollLeft+100;
    };

    const onClickChevronLeft = () => {
        let scrollLeft = listRef.current.scrollLeft;
        listRef.current.scrollLeft = scrollLeft - 100;
    };

    // onscroll listener to show and hide the arrows
    const scrollListener = () => {
        let scrollLeft = listRef.current.scrollLeft;
        let scrollWidth = listRef.current.scrollWidth;
        let elWidth = Math.ceil(listRef.current.getBoundingClientRect().width);
        // if the scroll width is equal or grater than element width + scroll left distance
        // hide the right arrow
        if((elWidth+scrollLeft) >= scrollWidth) {
            setHideArrowRight(true);
        } else if(hideArrowRight) {
            setHideArrowRight(false);
        }
        // if the scroll left distance is 0 hide the left arrow
        if(scrollLeft === 0) {
            setShowArrowLeft(false);
        } else if(scrollLeft > 0 && !showArrowLeft) {
            setShowArrowLeft(true);
        }
    };

    useEffect(() => {
        // check if the scroll width is greater than the list container width
        // if so, it means its overflowing and the arrow can be shown
        let scrollWidth = Math.ceil(listRef.current.scrollWidth);
        let containerWidth = Math.ceil(listRef.current.getBoundingClientRect().width);
        if((scrollWidth > containerWidth) && !canShowRightArrow) {
            setCanShowArrow(true);
        }
    }, [listRef, canShowRightArrow]);

    return (
        <div>
            <Heading>Approved Images ({approvedImages.length})</Heading>
            <ListContainer>
                {
                    showArrowLeft &&
                    <StyledSVGContainer>
                        <StyledChevronLeft
                            onClick={onClickChevronLeft}
                        />
                    </StyledSVGContainer>
                }
                <ListWrapper ref={listRef} onScroll={scrollListener}>
                    {
                        approvedImages.length > 0 ?
                            approvedImages.map((item, i) => {
                                return <ImageWrapper key={i}>
                                    <CheckWrapper><StyledCheck /></CheckWrapper>
                                    <Image src={item.url} alt={item.alt} />
                                </ImageWrapper>
                            }) : <Placeholder
                                showLoading={showLoading}
                                type="approved"
                                onClick={onClickPlus}
                            />
                    }
                </ListWrapper>
                {
                    canShowRightArrow &&
                        (!hideArrowRight &&
                            <StyledSVGContainer>
                                <StyledChevronRight
                                    onClick={onClickChevronRight}
                                />
                            </StyledSVGContainer>)
                }
            </ListContainer>
        </div>
    )
}

const Heading = styled.h3`
    font-size: 18px;
    color: ${PrimaryColor}
`;

const ListWrapper = styled.div`
    display: flex;
    gap: 15px;
    overflow: scroll;
    scroll-behavior: smooth;
`;

const Image = styled.img`
    width: ${smallImageWidth-2}px;
    height: ${smallImageHeight-2}px;
    border-radius: 8px;
    border: 1px solid ${BorderColor};
    object-fit: contain;
    background: ${BgColor};
`;

const ImageWrapper = styled.div`
    position: relative;
    border-radius: 8px;
`;

const CheckWrapper = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    border-radius: 6px;
    padding: 2px 4px;
    background: ${FadedBgColor}
`;

const StyledCheck = styled(Check)`
    width: 15px;
    height: 15px;
    color: ${PrimaryColor}
`;

const ListContainer = styled.div`
    display: flex;
    align-items: center;
`;

const StyledChevronRight = styled(ChevronRight)`
    width: 40px;
    height: 40px;
    cursor: pointer;
    color: ${PrimaryColor}
`;

const StyledSVGContainer = styled.div`
    width: 50px;
`;

const StyledChevronLeft = styled(ChevronLeft)`
    width: 40px;
    height: 40px;
    cursor: pointer;
    color: ${PrimaryColor}

`;

export default ApprovedImagesContainer;