import React from "react";
import {shallow, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Divider from "./Divider";

configure({adapter: new Adapter()});

describe("unit test for divider", () => {
    it("renders divider", () => {
        let wrapper = shallow(<Divider />);
        expect(wrapper.hasClass('divider')).toBe(true);
    })
})