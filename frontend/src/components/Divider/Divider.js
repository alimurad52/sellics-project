import React from 'react';
import styled from "styled-components";
import {DividerColor} from "../../utils/const";

const Divider = () => {
    return (
        <Hr className="divider" />
    )
}

const Hr = styled.hr`
    margin: 20px 0;
    border: 1px solid ${DividerColor}
`;

export default Divider;