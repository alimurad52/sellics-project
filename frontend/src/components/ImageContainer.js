import React from "react";
import styled from "styled-components";
import {BgColor, BorderColor, FadedBgColor} from "../utils/const";

const ImageContainer = ({imageList}) => {
    let image = imageList[0];
    return (
        <div>
            <ImageWrapper>
                <Image
                    src={image.urls.regular}
                    alt={image.alt_description}
                />
                <CreditContainer>
                    <p>
                        Photo by <a
                            rel="noreferrer"
                            target="_blank"
                            href={`${image.user.html}?utm_source=Sellics&utm_medium=referral`}
                        >
                            {image.user.name}
                        </a> on <a
                            rel="noreferrer"
                            href="https://unsplash.com/?utm_source=Sellics&utm_medium=referral"
                            target="_blank"
                        >
                            Unsplash
                        </a>
                    </p>
                </CreditContainer>
            </ImageWrapper>
        </div>
    )
};

const ImageWrapper = styled.div`
    background-color: ${BgColor};
    border-radius: 8px;
    border: 1px solid ${BorderColor};
    box-sizing: border-box;
    height: 500px;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
`;

const Image = styled.img`
    max-height: 498px;
    max-width: 100%;
    object-fit: contain;
`;

const CreditContainer = styled.div`
    position: absolute;
    bottom: 0;
    right: 0;
    padding: 0 5px;
    background:${FadedBgColor};
`;

export default ImageContainer;