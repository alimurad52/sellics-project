import React from 'react';
import ReactDOM from 'react-dom';
import Home from "./layout/Home";
import './index.css';
import Navbar from "./components/Navbar/Navbar";
import styled from "styled-components";
import {PrimaryColor} from "./utils/const";
import { store, persistor } from "./store/store";
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';

const AppContainer = styled.div`
    max-width: 761px;
    margin-left: 50%;
    transform: translate(-50%);
    border-radius: 8px;
    background: white;
    height: 100%;
    box-shadow: 6px 10px 15px -6px rgba(0,0,0,1);
    @media(max-width: 1024px) {
        margin: 0;
        transform: none;
        max-width: 100%;
    }
`;

const AppWrapper = styled.div`
    background: ${PrimaryColor};
    padding: 40px;
    @media(max-width: 1024px) {
        padding: 0;
    }
`;

ReactDOM.render(
    <Provider store={store}>
        <PersistGate persistor={persistor} loading={null}>
            <AppWrapper>
                <AppContainer>
                    <Navbar />
                    <Home />
                </AppContainer>
            </AppWrapper>
        </PersistGate>
    </Provider>,
  document.getElementById('root')
);
